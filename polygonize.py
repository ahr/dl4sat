from scipy import spatial
from operator import itemgetter
from itertools import groupby
import cv2
import numpy as np
import sys


def linear_contours(img, THRESHOLD):
    # find contours
    _, contours, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    new_contours = []
    for c, contour in enumerate(contours):
        indices = []
        print(c)
        length = len(contour)
        idx = 0
        print(length)
        while(idx < length):
            if idx == length - 2:
                dist = spatial.distance.euclidean(contour[idx],
                                                  contour[idx + 1])
                flagged_indices = np.asarray([idx + 1]) if dist < THRESHOLD else None
                indices.append(flagged_indices)
                idx = idx + 1
                continue
            elif (idx == length - 1):
                idx = idx + 1
                continue
            else:
                dist = spatial.distance.cdist(contour[idx],
                                              np.squeeze(contour[idx + 1:]))
                flagged_indices = (np.where(dist < THRESHOLD))[1] + idx + 1
                flagged_index_groups = []
                for k, g in groupby(enumerate(flagged_indices), lambda (i, x): i - x):
                    flagged_index_groups.append(map(itemgetter(1), g))
                idx = flagged_index_groups[0][-1] + 1
                indices.append(flagged_index_groups[0])

        total_indices = np.unique(np.concatenate(indices))
        new_contour = np.delete(contour, total_indices, 0)
        print(len(new_contour))
        new_contours.append(new_contour)
    return new_contours


def draw_contours(img, new_contours):
    color_img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    newimg = cv2.drawContours(color_img, new_contours, -1, (0, 255, 0), 2)
    return newimg


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python polygonize.py [threshold]")
        sys.exit()
    THRESHOLD = int(sys.argv[1])
    img = cv2.imread('class_map3.png', 0)
    new_contours = linear_contours(img, THRESHOLD)
    contoured = draw_contours(img, new_contours)
    cv2.imwrite('contoured' + sys.argv[1] + '.png', contoured)
