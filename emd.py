def emd(x, y):
    emd = 0
    temp = 0
    for i in range(len(x)):
        if i < len(y):
            temp = (x[i] + temp) - y[i]
        else:
            temp = (x[i] + temp)
        emd += abs(temp)
    return emd


if __name__ == '__main__':
    a = [3, 2, 1]
    b = [1, 2, 3]
    print(emd(a, b))
